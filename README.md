### Para subir o projeto
É preciso ter o maven na maquina, rodar:

mvn spring-boot:run

### Para testar o projeto

mvn test

### Endponts

#### Adicionar Room
```bash
curl -v -X POST -H "Content-Type: application/json" --data '{"name":"Sala Bjorn"}' http://localhost:8080/rooms
```
#### Altera Room
```bash
curl -v -X PUT -H "Content-Type: application/json" --data '{"name":"Sala Flock"}' http://localhost:8080/rooms/1
```
#### Deleta Room 
```bash
curl -v -X DELETE http://localhost:8080/rooms/1
```
#### Adicionar Booking
```bash
curl -v -X POST -H "Content-Type: application/json" --data '{"title":"Grooming","init":"2020-09-30T14:58","end":"2020-09-30T17:58"}' http://localhost:8080/rooms/2/bookings
```
#### Altera Booking
```bash
curl -v -X PUT -H "Content-Type: application/json" --data '{"title":"Daily","init":"2020-09-30T14:58","end":"2020-09-30T17:58"}' http://localhost:8080/rooms/2/bookings/1
```
#### Deleta Booking 
```bash
curl -v -X DELETE http://localhost:8080/rooms/2/bookings/1
```
#### Busca por periodo
```bash
curl -v -X GET 'http://localhost:8080/rooms/bookings?init=2018-01-05T17:00&end=2018-12-05T17:00'
```
#### Busca por Room e periodo
```bash
curl -v -X GET 'http://localhost:8080/rooms/2/bookings?init=2018-01-05T17:00&end=2018-12-05T17:00'
```
