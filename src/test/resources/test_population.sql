insert into room (id, name) values (1, 'Sala Vermelha');
insert into room (id, name) values (2, 'Sala Amarela');
insert into room (id, name) values (3, 'Sala Verde');

insert into booking (id, title, room_id, init_time, end_time) values (1, 'Grooming', 2, '2018-09-05 18:00:00', '2018-09-05 19:00:11');

insert into booking (id, title, room_id, init_time, end_time) values (2, 'Grooming', 3, '2018-09-06 18:30:00', '2018-09-06 19:15:11');
insert into booking (id, title, room_id, init_time, end_time) values (3, 'Planning', 3, '2018-09-07 15:00:00', '2018-09-07 15:14:11');
insert into booking (id, title, room_id, init_time, end_time) values (4, 'Retro', 3, '2018-09-07 18:30:00', '2018-09-07 19:50:11');
insert into booking (id, title, room_id, init_time, end_time) values (5, 'Daily', 3, '2018-09-08 09:00:00', '2018-09-08 10:00:11');
