package br.com.hannaf.meetingroom.room.booking;

import br.com.hannaf.meetingroom.room.Room;
import br.com.hannaf.meetingroom.room.RoomRepository;
import br.com.hannaf.meetingroom.room.booking.Booking;
import br.com.hannaf.meetingroom.room.booking.repository.BookingRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;

import static java.time.LocalDateTime.now;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Sql(scripts = "classpath:test_population.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "classpath:clean_population.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class BookingControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private BookingRepository bookingRepository;

    @Test
    public void shouldBookRoom() throws Exception {
        LocalDateTime init = now();
        LocalDateTime end = init.plus(Duration.ofMinutes(30));

        String content = "{\"title\":\"Grooming\",\"init\":\"" + init + "\",\"end\":\"" + end + "\"}";

        mvc.perform(post("/rooms/1/bookings").contentType(APPLICATION_JSON).content(content))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json("{\"id\":6,\"title\":\"Grooming\",\"init\":\"" + init + "\",\"end\":\"" + end + "\"}"));

        Optional<Booking> booking = bookingRepository.findById(6l);

        assertTrue(booking.isPresent());
        assertEquals("Grooming", booking.get().getTitle());
        assertEquals(init, booking.get().getPeriod().getInit());
        assertEquals(end, booking.get().getPeriod().getEnd());
        assertEquals(new Long(1), booking.get().getRoom().getId());
    }

    @Test
    public void shouldReceiveBadRequestForInvalidPeriod() throws Exception {
        LocalDateTime init = now();
        LocalDateTime end = init.minus(Duration.ofMinutes(10));

        String content = "{\"title\":\"Grooming\",\"init\":\"" + init + "\",\"end\":\"" + end + "\"}";

        mvc.perform(post("/rooms/1/bookings").contentType(APPLICATION_JSON).content(content))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json("{\"message\":\"Final date [" + end + "] must be bigger than initial [" + init  + "]\"}"));
    }

    @Test
    public void shouldReceiveBadRequestForConflictingBooking() throws Exception {
        LocalDateTime init = now();
        LocalDateTime end = init.plus(Duration.ofHours(3));

        Optional<Room> room = roomRepository.findById(1l);

        Booking conflict = Booking.createWithTitle("Conflict").withInit(init).withEnd(end).andMinimumDuration(Duration.ofMinutes(1)).buildFor(room.get());

        bookingRepository.save(conflict);

        String content = "{\"title\":\"Grooming\",\"init\":\"" + init + "\",\"end\":\"" + end.plus(Duration.ofMinutes(15)) + "\"}";

        mvc.perform(post("/rooms/1/bookings").contentType(APPLICATION_JSON).content(content))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json("{\"message\":\"There is already a booking in this range\",\"conflicts\":{\"bookings\":[{\"id\":6,\"title\":\"Conflict\",\"init\":\"" + init + "\",\"end\":\"" + end + "\",\"room\":{\"id\":1,\"name\":\"Sala Vermelha\"}}]}}"));
    }

    @Test
    public void shouldUpdateBooking() throws Exception {
        LocalDateTime init = now();
        LocalDateTime end = init.plus(Duration.ofMinutes(30));

        String content = "{\"title\":\"Board\",\"init\":\"" + init + "\",\"end\":\"" + end + "\"}";

        mvc.perform(put("/rooms/1/bookings/1").contentType(APPLICATION_JSON).content(content))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json("{\"id\":1,\"title\":\"Board\",\"init\":\"" + init + "\",\"end\":\"" + end + "\"}"));

        Optional<Booking> updatedBooking = bookingRepository.findById(1l);

        assertTrue(updatedBooking.isPresent());
        assertEquals("Board", updatedBooking.get().getTitle());
        assertEquals(init, updatedBooking.get().getPeriod().getInit());
        assertEquals(end, updatedBooking.get().getPeriod().getEnd());
        assertEquals(new Long(1), updatedBooking.get().getRoom().getId());
    }

    @Test
    public void shouldRemoveBooking() throws Exception {
        mvc.perform(delete("/rooms/2/bookings/1"))
                .andExpect(status().isNoContent());

        assertTrue(roomRepository.findById(2l).isPresent());
        assertFalse(bookingRepository.findById(1l).isPresent());
    }

    @Test
    public void shouldReceiveEmptyListForRoomWithoutBookings() throws Exception {
        mvc.perform(get("/rooms/1/bookings").param("init", "2018-09-05T17:00").param("end", "2018-09-07T17:00"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{\"bookings\":[]}"));

    }

    @Test
    public void shouldReceiveBookingsByRoom() throws Exception {
        mvc.perform(get("/rooms/2/bookings").param("init", "2018-08-05T17:00").param("end", "2018-10-07T17:00"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{\"bookings\":[{\"id\":1,\"title\":\"Grooming\",\"init\":\"2018-09-05T18:00\",\"end\":\"2018-09-05T19:00:11\",\"room\":{\"id\":2,\"name\":\"Sala Amarela\"}}]}"));

    }

    @Test
    public void shouldReceiveBookingsByRoomWithRestrictedPeriod() throws Exception {
        mvc.perform(get("/rooms/3/bookings").param("init", "2018-09-05T17:00").param("end", "2018-09-07T17:00"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{\"bookings\":[{\"id\":2,\"title\":\"Grooming\",\"init\":\"2018-09-06T18:30\",\"end\":\"2018-09-06T19:15:11\",\"room\":{\"id\":3,\"name\":\"Sala Verde\"}},{\"id\":3,\"title\":\"Planning\",\"init\":\"2018-09-07T15:00\",\"end\":\"2018-09-07T15:14:11\",\"room\":{\"id\":3,\"name\":\"Sala Verde\"}}]}"));
    }


    @Test
    public void shouldReceiveBookingsWithRestrictedPeriod() throws Exception {
        mvc.perform(get("/rooms/bookings").param("init", "2018-09-05T17:00").param("end", "2018-09-07T15:00"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{\"bookings\":[{\"id\":1,\"title\":\"Grooming\",\"init\":\"2018-09-05T18:00\",\"end\":\"2018-09-05T19:00:11\",\"room\":{\"id\":2,\"name\":\"Sala Amarela\"}},{\"id\":2,\"title\":\"Grooming\",\"init\":\"2018-09-06T18:30\",\"end\":\"2018-09-06T19:15:11\",\"room\":{\"id\":3,\"name\":\"Sala Verde\"}}]}"));
    }

}
