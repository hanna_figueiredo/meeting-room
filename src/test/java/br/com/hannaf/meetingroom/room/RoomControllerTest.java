package br.com.hannaf.meetingroom.room;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static java.time.LocalDateTime.now;
import static org.junit.Assert.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Sql(scripts = "classpath:test_population.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "classpath:clean_population.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class RoomControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private RoomRepository roomRepository;

    @Test
    public void shouldCreateNewRoom() throws Exception {
        mvc.perform(post("/rooms").contentType(APPLICATION_JSON).content("{\"name\":\"Sala Azul\"}"))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json("{\"id\":4,\"name\":\"Sala Azul\"}"));

        Optional<Room> room = roomRepository.findById(4l);

        assertTrue(room.isPresent());
        assertEquals("Sala Azul", room.get().getName());
    }

    @Test
    public void shouldReturnBadRequestWhenNewRoomHasSameNameAsExistingOne() throws Exception {
        mvc.perform(post("/rooms").contentType(APPLICATION_JSON).content("{\"name\":\"Sala Amarela\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json("{\"message\":\"Room with name Sala Amarela alread exists\"}"));
    }

    @Test
    public void shouldUpdateRoom() throws Exception {
        mvc.perform(put("/rooms/1").contentType(APPLICATION_JSON).content("{\"name\":\"Sala Azul\"}"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{\"id\":1,\"name\":\"Sala Azul\"}"));

        Optional<Room> room = roomRepository.findById(1l);

        assertTrue(room.isPresent());
        assertEquals("Sala Azul", room.get().getName());
    }

    @Test
    public void shouldDeleteRoom() throws Exception {
        mvc.perform(delete("/rooms/2").param("init", "2018-01-05T17:00").param("end", "2018-01-07T15:00"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void shouldReceiveNotFoundWhenTryingToNonexistentDeleteRoom() throws Exception {
        mvc.perform(delete("/rooms/4").param("init", "2018-01-05T17:00").param("end", "2018-01-07T15:00"))
                .andExpect(status().isNotFound());
    }

}
