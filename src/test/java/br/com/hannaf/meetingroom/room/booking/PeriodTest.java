package br.com.hannaf.meetingroom.room.booking;

import br.com.hannaf.meetingroom.room.exceptions.InvalidPeriodException;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class PeriodTest {

    @Test
    public void shouldCreatePeriod() {
        LocalDateTime init = LocalDateTime.of(2018, 01, 02, 15, 00);
        LocalDateTime end = LocalDateTime.of(2018, 01, 02, 15, 10);

        Period period = Period.createWithInitialDate(init)
                .andEnd(end)
                .buildWithMinimumDuration(Duration.ofMinutes(10));

        assertEquals(init, period.getInit());
        assertEquals(end, period.getEnd());
    }

    @Test
    public void shouldThrowExceptionWhenInitDateIsEqualEndDate() {
        try {
            LocalDateTime init = LocalDateTime.of(2018, 01, 02, 15, 00);
            LocalDateTime end = LocalDateTime.of(2018, 01, 02, 15, 00);

            Period period = Period.createWithInitialDate(init)
                    .andEnd(end)
                    .buildWithMinimumDuration(Duration.ofMinutes(10));
        } catch(InvalidPeriodException e) {
            assertEquals("Final date [2018-01-02T15:00] must be bigger than initial [2018-01-02T15:00]", e.getMessage());
        }
    }

    @Test
    public void shouldThrowExceptionWhenInitDateIsBeforeEndDate() {
        try {
            LocalDateTime init = LocalDateTime.of(2018, 01, 02, 15, 00);
            LocalDateTime end = LocalDateTime.of(2018, 01, 02, 14, 59);

            Period period = Period.createWithInitialDate(init)
                    .andEnd(end)
                    .buildWithMinimumDuration(Duration.ofMinutes(10));
        } catch(InvalidPeriodException e) {
            assertEquals("Final date [2018-01-02T14:59] must be bigger than initial [2018-01-02T15:00]", e.getMessage());
        }
    }

    @Test
    public void shouldThrowExceptionWhenPeriodIsLessThenMinimumDuration() {
        try {
            LocalDateTime init = LocalDateTime.of(2018, 01, 02, 15, 00);
            LocalDateTime end = LocalDateTime.of(2018, 01, 02, 15, 9);

            Period period = Period.createWithInitialDate(init)
                    .andEnd(end)
                    .buildWithMinimumDuration(Duration.ofMinutes(10));
        } catch(InvalidPeriodException e) {
            assertEquals("Period duration must be bigger than PT10M. Init 2018-01-02T15:00, end 2018-01-02T15:09", e.getMessage());
        }
    }

}