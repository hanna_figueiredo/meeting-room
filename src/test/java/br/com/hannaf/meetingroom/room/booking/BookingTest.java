package br.com.hannaf.meetingroom.room.booking;

import br.com.hannaf.meetingroom.room.Room;
import br.com.hannaf.meetingroom.room.exceptions.InvalidPeriodException;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BookingTest {

    @Test
    public void shouldCreateBooking() {
        String title = "Planning";
        LocalDateTime init = LocalDateTime.now();
        LocalDateTime end = init.plus(Duration.ofHours(1));
        Room room = new Room();

        Booking booking = Booking.createWithTitle(title).withInit(init).withEnd(end).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);

        assertNull(booking.getId());
        assertEquals(title, booking.getTitle());
        assertEquals(init, booking.getPeriod().getInit());
        assertEquals(end, booking.getPeriod().getEnd());
        assertEquals(room, booking.getRoom());
    }

    @Test
    public void shouldUpdateBooking() {
        Long bookingId = 1l;
        String title = "Planning";
        LocalDateTime init = LocalDateTime.now();
        LocalDateTime end = init.plus(Duration.ofHours(1));
        Room room = new Room();

        Booking booking = Booking.update(bookingId).withTitle(title).withInit(init).withEnd(end).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);

        assertEquals(bookingId, booking.getId());
        assertEquals(title, booking.getTitle());
        assertEquals(init, booking.getPeriod().getInit());
        assertEquals(end, booking.getPeriod().getEnd());
        assertEquals(room, booking.getRoom());
    }

    @Test
    public void shouldThrowExceptionWhenInitDateIsBeforeCurrentDate() {
        String title = "Planning";
        LocalDateTime init = LocalDateTime.now().minus(Duration.ofDays(1));
        LocalDateTime end = LocalDateTime.now();

        try {
            Booking.createWithTitle(title).withInit(init).withEnd(end).andMinimumDuration(Duration.ofMinutes(30)).buildFor(new Room());
        } catch (InvalidPeriodException e) {
            assertEquals("Init date of booking can not be before current date", e.getMessage());
        }
    }

}