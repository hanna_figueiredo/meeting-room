package br.com.hannaf.meetingroom.room.booking;

import br.com.hannaf.meetingroom.room.Room;
import br.com.hannaf.meetingroom.room.RoomRepository;
import br.com.hannaf.meetingroom.room.booking.repository.BookingRepository;
import br.com.hannaf.meetingroom.room.exceptions.PeriodConflictException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Duration;
import java.time.LocalDateTime;

import static org.junit.Assert.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@Sql(scripts = "classpath:clean_population.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class SchedulerTest {

    @Autowired
    private Scheduler scheduler;

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private LocalDateTime bookingInit = LocalDateTime.now().plus(Duration.ofDays(1));

    private LocalDateTime bookingEnd = bookingInit.plus(Duration.ofHours(3));

    private Room room;

    @Before
    public void setup() {
        String title = "Planning";
        room = roomRepository.save(new Room("1"));

        Booking booking = Booking.createWithTitle(title).withInit(bookingInit).withEnd(bookingEnd).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);
        bookingRepository.save(booking);

        Booking booking2 = Booking.createWithTitle(title).withInit(bookingInit.plus(Duration.ofHours(10))).withEnd(bookingEnd.plus(Duration.ofHours(10))).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);
        bookingRepository.save(booking2);

        Booking booking3 = Booking.createWithTitle(title).withInit(bookingInit.minus(Duration.ofHours(10))).withEnd(bookingEnd.minus(Duration.ofHours(10))).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);
        bookingRepository.save(booking3);
    }

    @Test
    public void shouldThrowExceptionIfHasABookBetweenNewDate() {
        expectedException.expect(PeriodConflictException.class);
        expectedException.expectMessage("There is already a booking in this range");

        LocalDateTime init = bookingInit.plus(Duration.ofMinutes(15));
        LocalDateTime end = bookingEnd.minus(Duration.ofMinutes(15));

        Booking newBooking = Booking.createWithTitle("Some").withInit(init).withEnd(end).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);
        scheduler.save(newBooking);
    }


    @Test
    public void shouldReceiveBadRequestForConflictingBooking() throws Exception {
        expectedException.expect(PeriodConflictException.class);
        expectedException.expectMessage("There is already a booking in this range");

        Booking newBooking = Booking.createWithTitle("Conflict").withInit(bookingInit).withEnd(bookingInit.plus(Duration.ofMinutes(10))).andMinimumDuration(Duration.ofMinutes(1)).buildFor(room);

        scheduler.save(newBooking);
    }

    @Test
    public void shouldThrowExceptionIfDataIsEqualsExistentBook() {
        expectedException.expect(PeriodConflictException.class);
        expectedException.expectMessage("There is already a booking in this range");

        Booking newBooking = Booking.createWithTitle("Some").withInit(bookingInit).withEnd(bookingEnd).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);

        scheduler.save(newBooking);
    }

    @Test
    public void shouldThrowExceptionIfInitDateIsEqualsExistentBookingAndEndDateIsAfter() {
        expectedException.expect(PeriodConflictException.class);
        expectedException.expectMessage("There is already a booking in this range");

        LocalDateTime end = bookingEnd.plus(Duration.ofMinutes(15));

        Booking newBooking = Booking.createWithTitle("Some").withInit(bookingInit).withEnd(end).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);

        scheduler.save(newBooking);
    }

    @Test
    public void shouldThrowExceptionIfInitDateIsBeforeExistentBookingAndEndDateIsEquals() {
        expectedException.expect(PeriodConflictException.class);
        expectedException.expectMessage("There is already a booking in this range");

        LocalDateTime init = bookingInit.minus(Duration.ofMinutes(15));

        Booking newBooking = Booking.createWithTitle("Some").withInit(init).withEnd(bookingEnd).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);

        scheduler.save(newBooking);
    }

    @Test
    public void shouldThrowExceptionIfNewDateIsBetweenABooking() {
        expectedException.expect(PeriodConflictException.class);
        expectedException.expectMessage("There is already a booking in this range");

        LocalDateTime init = bookingInit.minus(Duration.ofMinutes(15));
        LocalDateTime end = bookingEnd.plus(Duration.ofMinutes(15));

        Booking newBooking = Booking.createWithTitle("Some").withInit(init).withEnd(end).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);

        scheduler.save(newBooking);
    }

    @Test
    public void shouldThrowExceptionIfNewDateStartAfterABookingBegin() {
        expectedException.expect(PeriodConflictException.class);
        expectedException.expectMessage("There is already a booking in this range");

        LocalDateTime init = bookingInit.plus(Duration.ofMinutes(15));
        LocalDateTime end = bookingEnd.plus(Duration.ofMinutes(15));

        Booking newBooking = Booking.createWithTitle("Some").withInit(init).withEnd(end).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);

        scheduler.save(newBooking);
    }

    @Test
    public void shouldThrowExceptionIfNewDateFinishAfterABookingBegin() {
        expectedException.expect(PeriodConflictException.class);
        expectedException.expectMessage("There is already a booking in this range");

        LocalDateTime init = bookingInit.minus(Duration.ofMinutes(15));
        LocalDateTime end = bookingEnd.minus(Duration.ofMinutes(15));

        Booking newBooking = Booking.createWithTitle("Some").withInit(init).withEnd(end).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);

        scheduler.save(newBooking);
    }

    @Test
    public void shouldSavePeriodIfHasNoOverlapingWithExistentBookins() {
        LocalDateTime init = bookingEnd;
        LocalDateTime end = bookingEnd.plus(Duration.ofHours(6));

        Booking newBooking = Booking.createWithTitle("Some").withInit(init).withEnd(end).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);

        newBooking = scheduler.save(newBooking);

        assertNotNull(newBooking.getId());
    }

    @Test
    public void shouldSaveBookingWhenHasNoConflict() {
        LocalDateTime init = bookingInit.minus(Duration.ofHours(5));
        LocalDateTime end = bookingInit;

        Booking newBooking = Booking.createWithTitle("Some").withInit(init).withEnd(end).andMinimumDuration(Duration.ofMinutes(30)).buildFor(room);

        newBooking = scheduler.save(newBooking);

        assertNotNull(newBooking.getId());
    }

}
