package br.com.hannaf.meetingroom.room.booking;

import br.com.hannaf.meetingroom.room.Room;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Duration;
import java.time.LocalDateTime;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookingToBookingDTOConverterTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Booking booking;

    @Test
    public void shouldConvertBookingToBookingDTO() {
        BookingToBookingDTOConverter converter = new BookingToBookingDTOConverter();

        LocalDateTime init = LocalDateTime.now();
        LocalDateTime end = init.plus(Duration.ofHours(1));

        Room room = new Room();
        room.setId(6l);
        room.setName("Sala Cinza");

        when(booking.getId()).thenReturn(1l);
        when(booking.getTitle()).thenReturn("Planning");
        when(booking.getPeriod().getInit()).thenReturn(init);
        when(booking.getPeriod().getEnd()).thenReturn(end);
        when(booking.getRoom()).thenReturn(room);

        BookingDTO bookingDTO = converter.convert(booking);

        assertEquals(booking.getId(), bookingDTO.getId());
        assertEquals(booking.getTitle(), bookingDTO.getTitle());
        assertEquals(booking.getPeriod().getInit(), bookingDTO.getInit());
        assertEquals(booking.getPeriod().getEnd(), bookingDTO.getEnd());
        assertEquals(room.getId(), bookingDTO.getRoom().getId());
        assertEquals(room.getName(), bookingDTO.getRoom().getName());
    }

}