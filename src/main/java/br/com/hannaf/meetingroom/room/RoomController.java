package br.com.hannaf.meetingroom.room;

import br.com.hannaf.meetingroom.room.booking.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Optional;

@RequestMapping("/rooms")
@RestController
public class RoomController {

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private RoomService roomService;

    @Autowired
    private BookingsToBookingsDTOConverter bookingsToBookingsDTOConverter;

    @Autowired
    private Scheduler scheduler;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RoomDTO save(@RequestBody @Valid RoomDTO room) {
        Room newRoom = roomService.save(new Room(room.getName()));

        return new RoomDTO().setId(newRoom.getId()).setName(newRoom.getName());
    }

    @PutMapping("/{roomId}")
    @ResponseStatus(HttpStatus.OK)
    public RoomDTO update(@PathVariable @NotNull Long roomId, @RequestBody @Valid RoomDTO room) {
        Room updatedRoom = roomService.save(new Room(roomId, room.getName()));

        return new RoomDTO().setId(updatedRoom.getId()).setName(updatedRoom.getName());
    }

    @DeleteMapping("/{roomId}")
    public ResponseEntity remove(@PathVariable @NotNull Long roomId) {
        Optional<Room> roomOptional = roomRepository.findById(roomId);

        if (roomOptional.isPresent()) {
            roomRepository.delete(roomOptional.get());

            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.notFound().build();
    }

}
