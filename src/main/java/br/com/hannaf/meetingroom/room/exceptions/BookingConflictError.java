package br.com.hannaf.meetingroom.room.exceptions;

import br.com.hannaf.meetingroom.room.booking.BookingsDTO;

public class BookingConflictError {

    private String message;

    private BookingsDTO conflicts;

    public BookingConflictError(String message, BookingsDTO conflicts) {
        this.message = message;
        this.conflicts = conflicts;
    }

    public String getMessage() {
        return message;
    }

    public BookingsDTO getConflicts() {
        return conflicts;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BookingConflictError{");
        sb.append("message='").append(message).append('\'');
        sb.append(", conflicts=").append(conflicts);
        sb.append('}');
        return sb.toString();
    }
}
