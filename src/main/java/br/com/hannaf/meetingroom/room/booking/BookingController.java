package br.com.hannaf.meetingroom.room.booking;

import br.com.hannaf.meetingroom.room.Room;
import br.com.hannaf.meetingroom.room.RoomRepository;
import br.com.hannaf.meetingroom.room.booking.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Optional;

@RequestMapping("/rooms")
@RestController
public class BookingController {

    @Autowired
    private ScheduleProperties scheduleProperties;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private BookingToBookingDTOConverter converter;

    @Autowired
    private BookingsToBookingsDTOConverter bookingsToBookingsDTOConverter;

    @Autowired
    private Scheduler scheduler;

    @PostMapping("/{roomId}/bookings")
    public ResponseEntity schedule(@PathVariable @NotNull Long roomId, @RequestBody @Valid BookingDTO bookingDto) {
        Optional<Room> room = roomRepository.findById(roomId);

        if (room.isPresent()) {
            Booking booking = Booking.createWithTitle(bookingDto.getTitle()).withInit(bookingDto.getInit()).withEnd(bookingDto.getEnd())
                    .andMinimumDuration(scheduleProperties.getMinimunDuration()).buildFor(room.get());

            BookingDTO dto = converter.convert(scheduler.save(booking));

            return ResponseEntity.status(HttpStatus.CREATED).body(dto);
        }

        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{roomId}/bookings/{bookingId}")
    public ResponseEntity updateSchedule(@PathVariable @NotNull Long roomId, @PathVariable @NotNull Long bookingId, @RequestBody @Valid BookingDTO bookingDto) {
        Optional<Room> room = roomRepository.findById(roomId);

        if (room.isPresent()) {
            Booking booking = Booking.update(bookingId).withTitle(bookingDto.getTitle()).withInit(bookingDto.getInit()).withEnd(bookingDto.getEnd())
                    .andMinimumDuration(scheduleProperties.getMinimunDuration()).buildFor(room.get());

            BookingDTO dto = converter.convert(scheduler.save(booking));

            return ResponseEntity.status(HttpStatus.CREATED).body(dto);
        }

        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{roomId}/bookings/{bookingId}")
    public ResponseEntity removeSchedule(@PathVariable @NotNull Long roomId, @PathVariable @NotNull Long bookingId) {
        if (roomRepository.findById(roomId).isPresent()) {
            Optional<Booking> booking = bookingRepository.findById(bookingId);

            if (booking.isPresent()) {
                bookingRepository.delete(booking.get());

                return ResponseEntity.noContent().build();
            }
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/{roomId}/bookings")
    public ResponseEntity list(@PathVariable @NotNull Long roomId, @RequestParam(value = "init") @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm") LocalDateTime init, @RequestParam("end") @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm") LocalDateTime end) {
        Optional<Room> room = roomRepository.findById(roomId);

        if (room.isPresent()) {
            BookingsDTO bookingsDTO = bookingsToBookingsDTOConverter.convert(scheduler.findBy(room.get(), init, end));

            return ResponseEntity.ok(bookingsDTO);
        }

        return ResponseEntity.notFound().build();
    }

    @GetMapping("/bookings")
    public ResponseEntity list(@RequestParam("init") @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm") LocalDateTime init, @RequestParam("end") @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm") LocalDateTime end) {
        BookingsDTO bookingsDTO = bookingsToBookingsDTOConverter.convert(scheduler.findBy(init, end));
        return ResponseEntity.ok(bookingsDTO);
    }
}
