package br.com.hannaf.meetingroom.room.booking;

import br.com.hannaf.meetingroom.room.exceptions.InvalidPeriodException;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.Duration;
import java.time.LocalDateTime;

@Embeddable
public class Period {

    private LocalDateTime initTime;

    private LocalDateTime endTime;

    Period() {}

    private Period(LocalDateTime initTime, LocalDateTime endTime) {
        this.initTime = initTime;
        this.endTime = endTime;
    }

    public static PeriodWithInitialDate createWithInitialDate(LocalDateTime initialTime) {
        return new PeriodWithInitialDate(initialTime);
    }

    public LocalDateTime getInit() {
        return initTime;
    }

    public LocalDateTime getEnd() {
        return endTime;
    }

    public static class PeriodWithInitialDate {

        private final LocalDateTime init;

        public PeriodWithInitialDate(LocalDateTime init) {
            this.init = init;
        }

        public PeriodWithInitialAndEndDate andEnd(LocalDateTime end) {
            if (!end.isAfter(init)) {
                throw new InvalidPeriodException(String.format("Final date [%s] must be bigger than initial [%s]", end, init));
            }

            return new PeriodWithInitialAndEndDate(init, end);
        }
    }

    public static class PeriodWithInitialAndEndDate {

        private final LocalDateTime init;
        private final LocalDateTime end;

        public PeriodWithInitialAndEndDate(LocalDateTime init, LocalDateTime end) {
            this.init = init;
            this.end = end;
        }

        public Period buildWithMinimumDuration(Duration duration) {
            if (init.plus(duration).isAfter(end)) {
                throw new InvalidPeriodException(String.format("Period duration must be bigger than %s. Init %s, end %s", duration, init, end));
            }

            return new Period(init, end);
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Period{");
        sb.append("initTime=").append(initTime);
        sb.append(", endTime=").append(endTime);
        sb.append('}');
        return sb.toString();
    }
}
