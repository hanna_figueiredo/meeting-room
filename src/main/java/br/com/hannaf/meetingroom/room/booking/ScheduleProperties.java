package br.com.hannaf.meetingroom.room.booking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
@ConfigurationProperties(prefix = "schedule")
public class ScheduleProperties {

    public int minimunBookingDurationInMinutes;

    public Duration getMinimunDuration() {
        return Duration.ofMinutes(minimunBookingDurationInMinutes);
    }

    public void setMinimunBookingDurationInMinutes(int minimunBookingDurationInMinutes) {
        this.minimunBookingDurationInMinutes = minimunBookingDurationInMinutes;
    }
}
