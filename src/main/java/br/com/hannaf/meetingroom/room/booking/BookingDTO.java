package br.com.hannaf.meetingroom.room.booking;

import br.com.hannaf.meetingroom.room.Room;
import br.com.hannaf.meetingroom.room.RoomDTO;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class BookingDTO {

    private Long id;

    @NotBlank
    private String title;

    @NotNull
    @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm")
    private LocalDateTime init;

    @NotNull
    @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm")
    private LocalDateTime end;

    private RoomDTO room;

    public BookingDTO() {}

    public BookingDTO(Long id, RoomDTO room) {
        this.id = id;
        this.room = room;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getInit() {
        return init;
    }

    public void setInit(LocalDateTime init) {
        this.init = init;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public RoomDTO getRoom() {
        return room;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BookingDTO{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", init=").append(init);
        sb.append(", end=").append(end);
        sb.append(", room=").append(room);
        sb.append('}');
        return sb.toString();
    }
}
