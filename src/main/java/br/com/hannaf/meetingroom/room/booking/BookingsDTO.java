package br.com.hannaf.meetingroom.room.booking;

import java.util.List;

public class BookingsDTO {

    private List<BookingDTO> bookings;

    public BookingsDTO(List<BookingDTO> bookings) {
        this.bookings = bookings;
    }

    public List<BookingDTO> getBookings() {
        return bookings;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BookingsDTO{");
        sb.append("bookings=").append(bookings);
        sb.append('}');
        return sb.toString();
    }
}
