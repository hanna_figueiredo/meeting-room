package br.com.hannaf.meetingroom.room.booking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookingsToBookingsDTOConverter implements Converter<List<Booking>, BookingsDTO> {

    @Autowired
    private BookingToBookingDTOConverter converter;

    @Override
    public BookingsDTO convert(List<Booking> bookings) {
        List<BookingDTO> list = bookings.stream().map(booking -> converter.convert(booking)).collect(Collectors.toList());

        return new BookingsDTO(list);
    }
}
