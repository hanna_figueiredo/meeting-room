package br.com.hannaf.meetingroom.room.booking.repository;

import br.com.hannaf.meetingroom.room.Room;
import br.com.hannaf.meetingroom.room.booking.Booking;
import br.com.hannaf.meetingroom.room.booking.Period;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.List;

class BookingRepositoryImpl implements BookingExtendedRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Booking> findConflictingRangesFor(LocalDateTime init, LocalDateTime end) {
        return em.createQuery("select b from " + Booking.class.getSimpleName() + " b where " + dataRangeQuery())
                .setParameter("initTime", init)
                .setParameter("endTime", end)
                .getResultList();
    }

    @Override
    public List<Booking> findConflictingRangesFor(Room room, LocalDateTime init, LocalDateTime end) {
        return em.createQuery("select b from " + Booking.class.getSimpleName() + " b where " +
                " b.room = :room and (" + dataRangeQuery() + ")")
                .setParameter("room", room)
                .setParameter("initTime", init)
                .setParameter("endTime", end)
                .getResultList();
    }

    private String dataRangeQuery() {
        return  "(b.period.initTime > :initTime AND b.period.initTime < :endTime) OR " +
                "(b.period.endTime > :initTime AND b.period.endTime < :endTime) OR " +
                "(b.period.initTime < :initTime AND b.period.endTime > :endTime) OR " +
                "(:initTime > b.period.initTime AND :initTime < b.period.endTime) OR " +
                "(:endTime > b.period.initTime AND :endTime < b.period.endTime) OR " +
                "(:initTime < b.period.initTime AND :endTime > b.period.endTime) OR " +
                "(:initTime = b.period.initTime AND :endTime = b.period.endTime)";
    }
}