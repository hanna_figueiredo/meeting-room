package br.com.hannaf.meetingroom.room.booking.repository;

import br.com.hannaf.meetingroom.room.Room;
import br.com.hannaf.meetingroom.room.booking.Booking;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface BookingRepository extends CrudRepository<Booking, Long>, BookingExtendedRepository {

    Optional<Booking> findById(Long bookingId);

    List<Booking> findByRoom(Room room);

    List<Booking> findByRoomAndPeriodInitTimeGreaterThanEqualAndPeriodEndTimeLessThanEqual(Room room, LocalDateTime init, LocalDateTime end);
}
