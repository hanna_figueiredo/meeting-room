package br.com.hannaf.meetingroom.room.exceptions;

public class InvalidPeriodException extends MeetingRoomApiException {

    public InvalidPeriodException(String message) {
        super(message);
    }
}
