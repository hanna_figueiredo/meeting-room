package br.com.hannaf.meetingroom.room;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class RoomDTO {

    private Long id;

    @NotBlank
    private String name;

    public Long getId() {
        return id;
    }

    public RoomDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public RoomDTO setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RoomDTO{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
