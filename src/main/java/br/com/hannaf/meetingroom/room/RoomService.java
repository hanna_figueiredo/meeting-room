package br.com.hannaf.meetingroom.room;

import br.com.hannaf.meetingroom.room.exceptions.MeetingRoomApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoomService {

    @Autowired
    private RoomRepository roomRepository;

    public Room save(Room room) {
        Optional<Room> preExistentRoom = roomRepository.findByName(room.getName());

        if (!preExistentRoom.isPresent()) {
            return roomRepository.save(room);
        } else {
            throw new MeetingRoomApiException(String.format("Room with name %s alread exists", room.getName()));
        }
    }

}
