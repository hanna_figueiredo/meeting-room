package br.com.hannaf.meetingroom.room.booking.repository;

import br.com.hannaf.meetingroom.room.Room;
import br.com.hannaf.meetingroom.room.booking.Booking;
import br.com.hannaf.meetingroom.room.booking.Period;

import java.time.LocalDateTime;
import java.util.List;

interface BookingExtendedRepository {
    List<Booking> findConflictingRangesFor(LocalDateTime init, LocalDateTime end);

    List<Booking> findConflictingRangesFor(Room room, LocalDateTime init, LocalDateTime end);
}
