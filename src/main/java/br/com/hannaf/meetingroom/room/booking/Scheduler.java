package br.com.hannaf.meetingroom.room.booking;

import br.com.hannaf.meetingroom.room.Room;
import br.com.hannaf.meetingroom.room.booking.repository.BookingRepository;
import br.com.hannaf.meetingroom.room.exceptions.PeriodConflictException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class Scheduler {

    @Autowired
    private BookingRepository bookingRepository;

    public Booking save(Booking booking) {
        List<Booking> conflicts = getConflicts(booking);

        if (conflicts.isEmpty()) {
            return bookingRepository.save(booking);
        } else {
            throw new PeriodConflictException("There is already a booking in this range", conflicts);
        }
    }

    public List<Booking> getConflicts(Booking booking) {
        List<Booking> conflicts = bookingRepository.findConflictingRangesFor(booking.getRoom(), booking.getPeriod().getInit(), booking.getPeriod().getEnd());
        if (booking.getId() != null) {
            return conflicts.stream().filter(conflict -> !conflict.getId().equals(booking.getId())).collect(Collectors.toList());
        }

        return conflicts;
    }

    public List<Booking> findBy(LocalDateTime init, LocalDateTime end) {
        return bookingRepository.findConflictingRangesFor(init, end);
    }

    public List<Booking> findBy(Room room, LocalDateTime init, LocalDateTime end) {
        return bookingRepository.findConflictingRangesFor(room, init, end);
    }
}
