package br.com.hannaf.meetingroom.room.exceptions;

import br.com.hannaf.meetingroom.room.booking.Booking;

import java.util.List;

public class PeriodConflictException extends MeetingRoomApiException {

    private final List<Booking> conflicts;

    public PeriodConflictException(String message, List<Booking> conflicts) {
        super(message);
        this.conflicts = conflicts;
    }

    public List<Booking> getConflicts() {
        return conflicts;
    }
}
