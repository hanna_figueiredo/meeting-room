package br.com.hannaf.meetingroom.room.booking;

import br.com.hannaf.meetingroom.room.Room;
import br.com.hannaf.meetingroom.room.RoomDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class BookingToBookingDTOConverter implements Converter<Booking, BookingDTO> {

    @Override
    public BookingDTO convert(Booking booking) {
        Room room = booking.getRoom();

        BookingDTO bookingDTO = new BookingDTO(booking.getId(), new RoomDTO().setId(room.getId()).setName(room.getName()));

        bookingDTO.setTitle(booking.getTitle());
        bookingDTO.setInit(booking.getPeriod().getInit());
        bookingDTO.setEnd(booking.getPeriod().getEnd());

        return bookingDTO;
    }
}
