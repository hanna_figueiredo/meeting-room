package br.com.hannaf.meetingroom.room;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RoomRepository extends CrudRepository<Room, Long> {

    Optional<Room> findByName(String name);

    Optional<Room> findById(Long roomId);
}
