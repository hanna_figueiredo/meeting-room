package br.com.hannaf.meetingroom.room.booking;

import br.com.hannaf.meetingroom.room.Room;
import br.com.hannaf.meetingroom.room.exceptions.InvalidPeriodException;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String title;

    @NotNull
    @ManyToOne
    private Room room;

    @Embedded
    @NotNull
    @Valid
    private Period period;

    private Booking() {}

    private Booking(Long id, String title, Period period, Room room) {
        this.id = id;
        this.title = title;
        this.period = period;
        this.room = room;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public static BookingWithId update(Long id) {
        return new BookingWithId(id);
    }

    public static class BookingWithId {

        private Long id;

        public BookingWithId(Long id) {
            this.id = id;
        }

        public BookingWithTitle withTitle(String title) {
            return new BookingWithTitle(id, title);
        }
    }

    public static BookingWithTitle createWithTitle(String title) {
        return new BookingWithTitle(null, title);
    }

    public static class BookingWithTitle {

        private final Long id;
        private final String title;

        public BookingWithTitle(Long id, String title) {
            this.id = id;
            this.title = title;
        }

        public BookingWithInit withInit(LocalDateTime init) {
            if (init.toLocalDate().isBefore(LocalDate.now())) {
                throw new InvalidPeriodException("Init date of booking can not be before current date");
            }

            return new BookingWithInit(id, title, init);
        }
    }

    public static class BookingWithInit {
        private final Long id;
        private final String title;
        private final LocalDateTime init;

        public BookingWithInit(Long id, String title, LocalDateTime init) {
            this.id = id;
            this.title = title;
            this.init = init;
        }

        public BookingWithEnd withEnd(LocalDateTime end) {
            return new BookingWithEnd(id, title, init, end);
        }

    }

    public static class BookingWithEnd {
        private final Long id;
        private final String title;
        private final LocalDateTime init;
        private final LocalDateTime end;

        public BookingWithEnd(Long id, String title, LocalDateTime init, LocalDateTime end) {
            this.id = id;
            this.title = title;
            this.init = init;
            this.end = end;
        }

        public BookingWithPeriod andMinimumDuration(Duration minimunDuration) {
            Period period = Period.createWithInitialDate(init).andEnd(end).buildWithMinimumDuration(minimunDuration);

            return new BookingWithPeriod(id, title, period);
        }
    }

    public static class BookingWithPeriod {
        private final Long id;
        private final String title;
        private final Period period;

        public BookingWithPeriod(Long id, String title, Period period) {
            this.id = id;
            this.title = title;
            this.period = period;
        }

        public Booking buildFor(Room room) {
            return new Booking(id, title, period, room);
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Booking{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", room=").append(room);
        sb.append(", period=").append(period);
        sb.append('}');
        return sb.toString();
    }

}
