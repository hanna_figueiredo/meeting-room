package br.com.hannaf.meetingroom.room.exceptions;

import br.com.hannaf.meetingroom.room.booking.BookingsToBookingsDTOConverter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ExceptionHandlingController extends ResponseEntityExceptionHandler {

    private static final Logger logger = Logger.getLogger(ExceptionHandlingController.class);

    @Autowired
    private BookingsToBookingsDTOConverter converter;

    @ExceptionHandler(MeetingRoomApiException.class)
    public final ResponseEntity<ErrorDetails> handle(MeetingRoomApiException ex, WebRequest request) {
        logger.error("[API][ERROR] " + ex.getMessage(), ex);

        return new ResponseEntity<>(new ErrorDetails(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PeriodConflictException.class)
    public final ResponseEntity<BookingConflictError> handle(PeriodConflictException ex, WebRequest request) {
        logger.error("[API][ERROR] " + ex.getMessage() + ", conflicts: " + ex.getConflicts(), ex);

        return new ResponseEntity<>(new BookingConflictError(ex.getMessage(), converter.convert(ex.getConflicts())), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorDetails> handle(Exception ex, WebRequest request) {
        logger.error("[UNEXPECTED][ERROR] " + ex.getMessage(), ex);

        return new ResponseEntity<>(new ErrorDetails("Unexpected error"), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}