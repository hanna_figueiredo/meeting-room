package br.com.hannaf.meetingroom.room.exceptions;

public class MeetingRoomApiException extends RuntimeException {

    public MeetingRoomApiException(String message) {
        super(message);
    }
}
